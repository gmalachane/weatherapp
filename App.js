import { StatusBar } from 'expo-status-bar';
import { FlatList, StyleSheet, Text, Image, View } from 'react-native';
import React, {useEffect, useState}from 'react';
import * as Location from 'expo-location';



export default function App() {
  const [data, setData] = useState(false);
  const [weeklyData, setWeeklyData] = useState(false);
  const API_Key = '251979bc5e8f37d167c3e8f9fe5d3e69';

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        fetchDataWeather("45.763420", "4.834277")
        return;
      }
      let location = await Location.getCurrentPositionAsync({});
      fetchDataWeather(location.coords.latitude, location.coords.longitude);
    })();
  }, [])

  const fetchDataWeather = (latitude, longitude) => {
    console.log("fetchdataweather");
    if(latitude && longitude) {
      fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&units=metric&appid=${API_Key}&lang=fr`).then(res => res.json()).then(data => {
      //fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&exclude=hourly,minutely&units=metric&appid=${API_Key}&lang=fr`).then(res => res.json()).then(data => {
      setData(data);
      console.log("api1: ",data);
      console.log("img", data.weather[0].icon)
      })
      fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&exclude=hourly,minutely&units=metric&appid=${API_Key}&lang=fr`).then(res => res.json()).then(weeklyData => {
      setWeeklyData(weeklyData);
      console.log("api2: ",weeklyData);
      })
    } else {
      console.log('erreur coordonnées');
    }
  }

  const d = new Date();
  let day = d.getDay() -1 ;
  let jours = ['Lundi', 'Mardi','Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']
  const _renderItem = ({ item, index }) => <View style={styles.weeklyDay}><Text>{jours[(day + index +1) % 7]}</Text><Text>{item.temp.day} °C</Text><Text>{item.weather[0].description}</Text></View>
 
  return (
    <View style={styles.container}>
      {console.log("mon app")}
      <Text style={styles.title}>Météo</Text>
      {data && weeklyData && (
        <>
      <View style={styles.current}>
        <Text style={styles.title_current}>{data.name}</Text>
        <Text>Aujourd'hui</Text>
        <Text>{data.main.temp} °C</Text>
        <Text>{data.weather[0].description}</Text>
        <Image source={{ uri: 'http://openweathermap.org/img/wn/'+ data.weather[0].icon +'@4x.png', }}/>
      </View>
      <FlatList data={weeklyData.daily} renderItem={_renderItem}></FlatList>
      </>
      )}
      <StatusBar style="auto"/>

    </View>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    backgroundColor: 'lightgrey',
    
  },
  current: {
    alignItems: 'center',
    lineHeight: 30,
    fontSize: 40,
    marginBottom: 10,
  },
  title: {
    color: 'blue',
    marginTop: 50,
    marginBottom: 30,
    fontSize: 50,
    
  }, 
  title_current: {
    marginBottom: 10,
    fontSize: 40,
  },
  weeklyDay: {
    marginBottom: 10,
    alignItems: 'center',
  }
  
});
